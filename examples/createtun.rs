use std::io;
extern crate tuntap;

use std::thread;

fn main() -> io::Result<()> {
    let mut tun = tuntap::TunDevice::new()?;
    tun.set_mtu(1400)?;
    tun.add_ip("192.168.178.0/24")?;
    tun.add_ip("2003:cd:3719:6500:3602:86ff:fe1f:7afe/64")?;
    tun.set_up()?;


    let clonee = tun.clone();
    let another_queue = tun.new_queue();
    println!("{:?}", tun);
    println!("{:?}", clonee);
    println!("{:?}", another_queue);
    thread::sleep_ms(5000);
    Ok(())
}
