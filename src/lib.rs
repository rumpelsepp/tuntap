use std::ffi::{CString, CStr};
use std::io;
use std::io::prelude::*;
use std::mem;
use std::os::unix::prelude::*;
use std::process::Command;
use std::ptr;

#[macro_use]
extern crate nix;
extern crate libc;
#[macro_use]
extern crate log;

use nix::fcntl::{open, fcntl, FcntlArg, OFlag};
use nix::sys::socket::{socket, AddressFamily, SockFlag, SockType};
use nix::sys::stat::Mode;
use nix::unistd::{read, write, close};

mod sys;


#[derive(Clone, Debug)]
pub struct TunDevice {
    name: String,
    mtu: libc::c_int,
    fd: libc::c_int,
}

impl TunDevice {
    pub fn new<S: ToString>(template: &S) -> io::Result<Self> {
        let mut name = template.to_string();
        let name_c = CString::new(name).unwrap();
        let fd = open("/dev/net/tun", OFlag::O_RDWR, Mode::empty()).unwrap();

        if name_c.as_bytes_with_nul().len() > libc::IFNAMSIZ {
            panic!("Name too long");
        }

        let mut ifreq: sys::ifreq;

        unsafe {
            ifreq = mem::zeroed();
            ptr::copy_nonoverlapping(name_c.as_ptr(), ifreq.ifr_name.as_mut_ptr(), name_c.as_bytes().len());
            ifreq.ifr_data.ifr_flags = libc::IFF_TUN | libc::IFF_NO_PI | libc::IFF_MULTI_QUEUE;
            sys::tunsetiff(fd, &mut ifreq as *mut _ as u64).unwrap();
            name = CStr::from_ptr(ifreq.ifr_name.as_ptr()).to_string_lossy().into();
        }

        Ok(Self{ name, fd, mtu: 1200 })
    }

    pub fn set_mtu(&mut self, mtu: i32) ->io::Result<()> {
        let fd = socket(AddressFamily::Inet, SockType::Stream, SockFlag::empty(), None).unwrap();

        unsafe {
            let mut ifreq: sys::ifreq = mem::zeroed();
            ptr::copy_nonoverlapping(self.name.as_ptr(), ifreq.ifr_name.as_mut_ptr() as *mut u8, self.name.as_bytes().len());
            ifreq.ifr_data.ifr_mtu = mtu;
            sys::siocsifmtu(fd, &ifreq).unwrap();
        }

        self.mtu = mtu;

        Ok(())
    }

    pub fn add_ip<'a, S: Into<&'a str>>(&self, addr: S) -> io::Result<()> {
        let addr = addr.into();
        let name = self.name.as_str();
        let cmd = Command::new("ip")
            .args(&["addr", "add", addr, "dev", name])
            .output()?;

        if !cmd.status.success() {
            if !cmd.stderr.is_empty() {
                let msg = String::from_utf8_lossy(&cmd.stderr).trim().to_string();
                return Err(io::Error::new(io::ErrorKind::InvalidInput, msg));
            }
        }

        Ok(())
    }

    pub fn set_up(&self) -> io::Result<()> {
        let fd = socket(AddressFamily::Inet, SockType::Stream, SockFlag::empty(), None).unwrap();

        unsafe {
            let mut ifreq: sys::ifreq = mem::zeroed();
            ptr::copy_nonoverlapping(self.name.as_ptr(), ifreq.ifr_name.as_mut_ptr() as *mut u8, self.name.as_bytes().len());
            ifreq.ifr_data.ifr_flags = (libc::IFF_UP | libc::IFF_RUNNING) as i16;
            sys::siocsifflags(fd, &ifreq).unwrap();
        }

        Ok(())
    }

    pub fn new_queue(&self) -> io::Result<Self> {
        let fd = open("/dev/net/tun", OFlag::O_RDWR, Mode::empty()).unwrap();
        let mut ifreq: sys::ifreq;

        unsafe {
            ifreq = mem::zeroed();
            ptr::copy_nonoverlapping(self.name.as_ptr(), ifreq.ifr_name.as_mut_ptr() as *mut u8, self.name.as_bytes().len());
            ifreq.ifr_data.ifr_flags = libc::IFF_TUN | libc::IFF_NO_PI | libc::IFF_MULTI_QUEUE;
            sys::tunsetiff(fd, &mut ifreq as *mut _ as u64).unwrap();
        }

        Ok(Self { name: self.name.clone(), fd: fd, mtu: self.mtu })
    }

    pub fn set_nonblocking(&self, nonblocking: bool) -> io::Result<()> {
        if nonblocking {
            match fcntl(self.fd, FcntlArg::F_SETFL(OFlag::O_NONBLOCK)) {
                Ok(_) => Ok(()),
                Err(nix::Error::Sys(_)) => Err(io::Error::last_os_error()),
                Err(e) => Err(io::Error::new(io::ErrorKind::Other, e)),
            }
        } else {
            let flags = fcntl(self.fd, FcntlArg::F_GETFL).unwrap();
            let mut oflag = OFlag::from_bits(flags).unwrap();
            oflag.toggle(OFlag::O_NONBLOCK);

            match fcntl(self.fd, FcntlArg::F_SETFL(oflag)) {
                Ok(_) => Ok(()),
                Err(nix::Error::Sys(_)) => Err(io::Error::last_os_error()),
                Err(e) => Err(io::Error::new(io::ErrorKind::Other, e)),
            }
        }
    }
}

impl Read for TunDevice {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        match read(self.fd, buf) {
            Err(nix::Error::Sys(_)) => Err(io::Error::last_os_error()),
            Err(e) => Err(io::Error::new(io::ErrorKind::Other, e)),
            Ok(v) => Ok(v),
        }
    }
}

impl Write for TunDevice {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        match write(self.fd, buf) {
            Err(nix::Error::Sys(_)) => Err(io::Error::last_os_error()),
            Err(e) => Err(io::Error::new(io::ErrorKind::Other, e)),
            Ok(v) => Ok(v),
        }
    }

	fn flush(&mut self) -> io::Result<()> {
		Ok(())
    }
}

impl AsRawFd for TunDevice {
    fn as_raw_fd(&self) -> RawFd {
        self.fd
    }
}

impl Drop for TunDevice {
    fn drop(&mut self) {
        if let Err(e) = close(self.as_raw_fd()) {
            warn!("{}", e);
        }
    }
}

#[cfg(feature = "mio")]
mod mio {
    extern crate mio;

    use std::io;
    use std::os::unix::io::AsRawFd;
    use self::mio::unix::EventedFd;

    impl mio::Evented for super::TunDevice {
        fn register(
            &self,
            poll: &mio::Poll,
            token: mio::Token,
            interest: mio::Ready,
            opts: mio::PollOpt,
        ) -> io::Result<()> {
            EventedFd(&self.as_raw_fd()).register(poll, token, interest, opts)
        }

        fn reregister(
            &self,
            poll: &mio::Poll,
            token: mio::Token,
            interest: mio::Ready,
            opts: mio::PollOpt,
        ) -> io::Result<()> {
            EventedFd(&self.as_raw_fd()).reregister(poll, token, interest, opts)
        }

        fn deregister(&self, poll: &mio::Poll) -> io::Result<()> {
            EventedFd(&self.as_raw_fd()).deregister(poll)
        }
    }
}
