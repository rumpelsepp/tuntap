#![allow(dead_code)]
use libc;

#[repr(C)]
#[derive(Copy, Clone)]
pub struct ifmap {
	pub mem_start: libc::c_ulong,
	pub mem_end:   libc::c_ulong,
	pub base_addr: libc::c_ushort,
	pub irq:       libc::c_uchar,
	pub dma:       libc::c_uchar,
	pub port:      libc::c_uchar,
}

#[repr(C)]
pub union ifr_data {
	pub ifr_addr:      libc::sockaddr,
	pub ifr_dstaddr:   libc::sockaddr,
	pub ifr_broadaddr: libc::sockaddr,
	pub ifr_netmask:   libc::sockaddr,
	pub ifr_hwaddr:    libc::sockaddr,

	pub ifr_flags:    libc::c_short,
	pub ifr_ivalue:   libc::c_int,
	pub ifr_mtu:      libc::c_int,
	pub ifr_map:      ifmap,
	pub ifr_slave:    [libc::c_char; libc::IFNAMSIZ],
	pub ifr_newname:  [libc::c_char; libc::IFNAMSIZ],
	pub ifr_data:     *mut libc::c_void,
}


// From netdevice(7):
//
// struct ifreq {
//    char ifr_name[IFNAMSIZ]; /* Interface name */
//    union {
//        struct sockaddr ifr_addr;
//        struct sockaddr ifr_dstaddr;
//        struct sockaddr ifr_broadaddr;
//        struct sockaddr ifr_netmask;
//        struct sockaddr ifr_hwaddr;
//        short           ifr_flags;
//        int             ifr_ifindex;
//        int             ifr_metric;
//        int             ifr_mtu;
//        struct ifmap    ifr_map;
//        char            ifr_slave[IFNAMSIZ];
//        char            ifr_newname[IFNAMSIZ];
//        char           *ifr_data;
//    };
// };
#[repr(C)]
pub struct ifreq {
    pub ifr_name: [libc::c_char; libc::IFNAMSIZ],
    pub ifr_data: ifr_data,
}

ioctl_read_bad!(siocgifflags, libc::SIOCGIFFLAGS, ifreq);
ioctl_write_ptr_bad!(siocsifflags, libc::SIOCSIFFLAGS, ifreq);
ioctl_read_bad!(siocgifaddr, libc::SIOCGIFADDR, ifreq);
ioctl_write_ptr_bad!(siocsifaddr, libc::SIOCSIFADDR, ifreq);
ioctl_read_bad!(siocgifdstaddr, libc::SIOCSIFADDR, ifreq);
ioctl_write_ptr_bad!(siocsifdstaddr, libc::SIOCSIFDSTADDR, ifreq);
ioctl_read_bad!(siocgifbrdaddr, libc::SIOCGIFBRDADDR, ifreq);
ioctl_write_ptr_bad!(siocsifbrdaddr, libc::SIOCSIFBRDADDR, ifreq);
ioctl_read_bad!(siocgifnetmask, libc::SIOCGIFNETMASK, ifreq);
ioctl_write_ptr_bad!(siocsifnetmask, libc::SIOCSIFNETMASK, ifreq);
ioctl_read_bad!(siocgifmtu, libc::SIOCGIFMTU, ifreq);
ioctl_write_ptr_bad!(siocsifmtu, libc::SIOCSIFMTU, ifreq);
// TODO: constant from libc
ioctl_write_ptr_bad!(siocsifname, 0x8923, ifreq);

ioctl_write_int!(tunsetiff, b'T', 202);
ioctl_write_int!(tunsetpersist, b'T', 203);
ioctl_write_int!(tunsetowner, b'T', 204);
ioctl_write_int!(tunsetgroup, b'T', 206);
